<?php

/**
 * Implements hook_views_default_views().
 */
function affiliate_ng_relationship_views_default_views() {
  $views = array();

  $view = new view;
  $view->name = 'affiliate_relationships';
  $view->description = '';
  $view->tag = 'Affiliate NG';
  $view->base_table = 'users';
  $view->human_name = 'Affiliate Relationships';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Relationships';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'act as an affiliate';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  /* Contextual filter: User: Affiliate Relationship (affiliate_relationship) */
  $handler->display->display_options['arguments']['affiliate_relationship_target_id']['id'] = 'affiliate_relationship_target_id';
  $handler->display->display_options['arguments']['affiliate_relationship_target_id']['table'] = 'field_data_affiliate_relationship';
  $handler->display->display_options['arguments']['affiliate_relationship_target_id']['field'] = 'affiliate_relationship_target_id';
  $handler->display->display_options['arguments']['affiliate_relationship_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['affiliate_relationship_target_id']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['affiliate_relationship_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['affiliate_relationship_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['affiliate_relationship_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: User: Active */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'user/%/affiliate/relationships';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Relationships';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $views[$view->name] = $view;

  return $views;
}
