<?php

/**
 * @file
 * Rules integration for affiliate_ng_relationship.
 *
 */

/**
 * Implementation of hook_rules_action_info().
 */
function affiliate_ng_relationship_rules_action_info() {
  return array(
    'affiliate_ng_relationship_save_affiliate_relationship' => array(
      'label' => t('Save affiliate relationship to profile'),
      'group' => t('Affiliate NG'),
      'parameter' => array(
        'user' => array(
		  'type' => 'user',
		  'label' => t('User that is logged in.'),
		),
		'overwrite' => array(
          'type' => 'boolean',
          'label' => t('Overwrite Affiliate Relationship'),
          'description' => t('If checked, any existing value will be overwritten with this affiliate.'),
          'default value' => FALSE,
        ),
      ),
    ),
  );
}

/**
 * save affiliate relationship to user profile from cookies
 */
function affiliate_ng_relationship_save_affiliate_relationship($user, $overwrite = FALSE){
  $relationship = $_COOKIE['affiliate_ng']['affiliate_uid'];
  if ($overwrite) {
	$user->affiliate_relationship['und'][0]['target_id'] = $relationship;
    user_save($user);
  }
  elseif (empty($user->affiliate_relationship['und'][0]['target_id'])) {
    $user->affiliate_relationship['und'][0]['target_id'] = $relationship;
    user_save($user);
  }
}
