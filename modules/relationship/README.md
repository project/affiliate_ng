Affiliate NG relationship module for Drupal 7.x.
Provides rules to add a permanent association with an affiliate to the user.
This module adds a field to the user object and rules to populate that field.

Questions, problems, or issues?  Please file a ticket here:
https://www.drupal.org/project/issues/affiliate_ng

REQUIREMENTS
------------
* Affiliate NG


INSTALLATION INSTRUCTIONS
-------------------------

1.  Login as site administrator.
2.  Enable the Affiliate NG Relationship module on the Administer -> Modules page
    (Under the "Affiliate-NG" category).

USAGE
-------------------------

This module provides a default rule and view that you can use, copy, or modify.
At the core of the module is a new action in rules that can be used to add the
affiliate code to the user object. This can be used to create custom rules to
react to different use-case scenarios. The provided rule and view are tagged in
both the rules UI and the views UI for easy location.