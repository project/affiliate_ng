<?php

/**
 * @file
 * Default rule configurations for Affiliate Relationship.
 */

function affiliate_ng_relationship_default_rules_configuration() {
  $rules = array();

  $rules_create_permanent_affiliate_relationship = '{ "rules_create_permanent_affiliate_relationship" : {
      "LABEL" : "Create permanent affiliate relationship",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "affiliate" ],
      "REQUIRES" : [ "affiliate_ng", "affiliate_ng_relationship", "rules" ],
      "ON" : [ "user_login" ],
      "IF" : [ { "affiliate_ng_is_active_campaign" : [] } ],
      "DO" : [
        { "affiliate_ng_relationship_save_affiliate_relationship" : { "user" : [ "account" ], "overwrite" : 0 } }
      ]
    }
  }';
  $rules['rules_create_permanent_affiliate_relationship'] = rules_import($rules_create_permanent_affiliate_relationship);
  return $rules;
}
